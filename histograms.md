### The 304 wavelength seems to have nicely distributed histograms. ###
![20141025_304_Histogram_Slopes.jpeg](https://bitbucket.org/repo/Ko6rr5/images/3341506316-20141025_304_Histogram_Slopes.jpeg)

![20120923_304_Histogram_Slopes.jpeg](https://bitbucket.org/repo/Ko6rr5/images/2398234650-20120923_304_Histogram_Slopes.jpeg)

### The 1600 does as well, although skewed. ###
![20130530_1600_Histogram_Slopes.jpeg](https://bitbucket.org/repo/Ko6rr5/images/617101032-20130530_1600_Histogram_Slopes.jpeg)


### However, it seems that for 193, and 211, (and also 171 - not pictured) there is a massive spike around a power-index value of 2.25. ###

![20120923_193_Histogram_Slopes.jpeg](https://bitbucket.org/repo/Ko6rr5/images/3919910368-20120923_193_Histogram_Slopes.jpeg)

![20120923_211_Histogram_Slopes.jpeg](https://bitbucket.org/repo/Ko6rr5/images/1704636407-20120923_211_Histogram_Slopes.jpeg)

![20130530_193_Histogram_Slopes.jpeg](https://bitbucket.org/repo/Ko6rr5/images/3916547411-20130530_193_Histogram_Slopes.jpeg)