# -*- coding: utf-8 -*-
"""
Created on Fri Sep 02 21:06:10 2016

@author: Brendan
"""

##############################################################################
### I will add the values.txt and seconds.txt files to my repository.
### As you will see, there are a bunch of figures and files that are saved
### when this program is run - which must have valid paths specified for 
### them.  
##############################################################################




################################################
### Find Baseline of Shuffled Data + Compare ###
################################################

import scipy.signal
import numpy as np
import matplotlib.pyplot as plt


# read in data 
v = np.loadtxt('D:/SDO/values.txt', dtype='float')
t = np.loadtxt('D:/SDO/seconds.txt', dtype='float')

# create 5-step moving average
vmod = np.convolve(v, np.ones((5,))/5, mode='valid');

# find difference in lengths between moving average array and original 
l = (len(v)-len(vmod))

# delete the difference in lengths from the original (Prof. Weigel mentioned making the arrays
# the same length should be done differently - this was just for now)
v2 = np.delete(v,range(0,l))

# subtract moving average from original
diff = v2-vmod

# make time array same length as values 
t2 = np.delete(t,range(0,l))


### shuffle the data and calculate the periodograms for each

N = 1000

# initialize arrays for shuffled data and baseline
shffl_array = [0 for i in range(0,N)]
baseline_MAX = [0 for i in range(0,N)]
baseline_AVG = [0 for i in range(0,len(diff))]

# shuffle data N times
for i in range(0,N):
    np.random.shuffle(diff)
    
    # Choosing points in frequency space depends on what frequencies we think we'll detect.
    # There's no point looking for 100 Hz signals when we're looking at T = 3 days periods.
    nout = 776 # number of frequency-space points at which to calculate the signal strength (output)
    periods = np.linspace(24, 780, nout)
    freqs = 1.0 / periods
    angular_freqs = 2 * np.pi * freqs

    pgram = scipy.signal.lombscargle(t2, diff, angular_freqs)
    
    shffl_array[i] = pgram  # add shuffled data to array to use for finding AVG or MAX


### Calculates the MAX power-value for each data shuffling + computes the 99.9%, 99%, and 50% percentiles.  
        
for i in range(0,N):
    baseline_MAX[i] = np.amax(shffl_array[i])
baseline_MAX = np.array(baseline_MAX)
baseline_MAX_999 = np.percentile(baseline_MAX,99.9)
baseline_MAX_99 = np.percentile(baseline_MAX,99)
baseline_MAX_50 = np.percentile(baseline_MAX,50)
baseline_MAX_999 = [baseline_MAX_999 for i in range(0,len(diff))]
baseline_MAX_99 = [baseline_MAX_99 for i in range(0,len(diff))]
baseline_MAX_50 = [baseline_MAX_50 for i in range(0,len(diff))]


### Calculates the AVERAGED baseline (Can probably get rid of)
"""
for j in range(0,len(diff)):
    for i in range(0,N):
        baseline_AVG[j] += shffl_array[i][j]
        
baseline_AVG = np.array(baseline_AVG)
baseline_AVG = baseline_AVG / N
"""


### Reset values to original
diff = v2-vmod

pgram = scipy.signal.lombscargle(t2, diff, angular_freqs)


# Plot the baseline percentiles vs the original data periodogram, save as jpeg 
plt.plot(periods, pgram)
plt.plot(periods, baseline_MAX_999, label='1 in 1000')
plt.plot(periods, baseline_MAX_99, label='1 in 100')
plt.plot(periods, baseline_MAX_50, label='1 in 2')
plt.xlabel('Period $T$ (seconds)')
plt.ylabel('Power')
plt.legend(loc='upper right', title='Likelihood of Occurrence')
plt.suptitle('Original Data vs Baseline Percentiles', fontsize=14, fontweight='bold')
plt.title('Percentiles generated from 1000 random data shuffles')
plt.savefig('D:/SDO/Shuffled180Sine/baseline_percentiles_versus_original.jpeg')
plt.close()


### Shuffle data one time and plot periodogram vs baseline, save as jpeg
np.random.shuffle(diff)

pgram = scipy.signal.lombscargle(t2, diff, angular_freqs)


# Plot the baseline percentiles vs the shuffled data periodogram, save as jpeg
plt.plot(periods, pgram)
plt.plot(periods, baseline_MAX_999, label='1 in 1000')
plt.plot(periods, baseline_MAX_99, label='1 in 100')
plt.plot(periods, baseline_MAX_50, label='1 in 2')
plt.xlabel('Period $T$ (seconds)')
plt.ylabel('Power')
plt.legend(loc='upper right', title='Likelihood of Occurrence')
plt.suptitle('Shuffled Data vs Baseline Percentiles', fontsize=14, fontweight='bold')
plt.title('Percentiles generated from 1000 random data shuffles')
plt.savefig('D:/SDO/Shuffled180Sine/baseline_percentiles_vs_shuffled.jpeg')
plt.close()
