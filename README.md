# README #

This repository is to keep track of the work done in PHYS 326 - Problems in Physics II.  A.K.A. "Pattern Recognition in Solar Images"

This project is taking place during the Fall 2016 semester at George Mason University.

***
[2013/05/30 193 Large Region Summary](https://bitbucket.org/bgallag6/phys326/src/2c042a71ba6d33130ec03622873eaeb0bdad4cdc/20130530_193.pdf?fileviewer=file-view-default)
***

***
[1600 - Rebin:2](https://bitbucket.org/bgallag6/phys326/src/e1447edf8922178406bc0618e471eaec8ca5fc13/1600_rebin2.md?at=master)

[1600 - Rebin:4](https://bitbucket.org/bgallag6/phys326/src/e1447edf8922178406bc0618e471eaec8ca5fc13/1600_rebin4.md?at=master)

[20130815 - 171 - original](https://bitbucket.org/bgallag6/phys326/src/e1447edf8922178406bc0618e471eaec8ca5fc13/20130815_171.md?at=master)

[20130815 - 171 - larger](https://bitbucket.org/bgallag6/phys326/src/e1447edf8922178406bc0618e471eaec8ca5fc13/20130815_171_larger.md?at=master)

[20130815 - 193 - larger](https://bitbucket.org/bgallag6/phys326/src/e1447edf8922178406bc0618e471eaec8ca5fc13/20130815_193_larger.md?at=master)

[1600 - 2 segments](https://bitbucket.org/bgallag6/phys326/src/e1447edf8922178406bc0618e471eaec8ca5fc13/20141025_1600_2seg.md?at=master)

[20161106 - Poles + Rebin:4](https://bitbucket.org/bgallag6/phys326/src/e1447edf8922178406bc0618e471eaec8ca5fc13/20161106_Poles_Rebin_6hr.md?at=master)

[histograms](https://bitbucket.org/bgallag6/phys326/src/f543acbdd715e0c07273030c54593115eb992516/histograms.md?at=master)


***
***
[20120923 - Datasets](https://bitbucket.org/bgallag6/phys326/src/0588a7226033614eab4895ebcbf61d3bd2870654/20120923.md?fileviewer=file-view-default)

[Heatmap-Spectra Tool](https://bitbucket.org/bgallag6/phys326/src/0a2619e7331781080b3593d89e50baf972f1f26f/heatmap_spectra_tool.md?fileviewer=file-view-default)

[304 - Filament](https://bitbucket.org/bgallag6/phys326/src/53584990010488be5e043dff81030d50040d55e2/20141025_304.md?fileviewer=file-view-default)

[193 - 'dogbox'](https://bitbucket.org/bgallag6/phys326/src/fb6fed89b29aa1e9db73a440e0b18edf1dcad5c3/20130530_193.md?fileviewer=file-view-default)

[PowerPoint from meeting with Jack](https://bitbucket.org/bgallag6/phys326/src/3e57d873685b5bee4f50d707e4bb5ea0de52cfb0/Meeting_PowerPoint.md?at=master&fileviewer=file-view-default)


***
***

*These two plots show the difference that the 'dogbox' method of scipy's least_squares makes vs the default 'trf'.  (First = 'trf', Second = 'dogbox').  To me, it seems that the 'dogbox' method is more-accurately fitting the slope, which had been causing a significant portion of the fitting issues that I was seeing.* 

![20130530_193A_3x3_6seg_seed_params_3i_3j_peak.jpeg](https://bitbucket.org/repo/Ko6rr5/images/2585496778-20130530_193A_3x3_6seg_seed_params_3i_3j_peak.jpeg)

![20130530_193A_3x3_6seg_seed_params_3i_3j_wide.jpeg](https://bitbucket.org/repo/Ko6rr5/images/1367183721-20130530_193A_3x3_6seg_seed_params_3i_3j_wide.jpeg)


*These two heatmaps are just to show how similar the results were for the 1600A dataset when using the original fitting method, versus the 'dogbox' method.*

![20130530_1600_heatmap_Gauss_Amp_final.jpeg](https://bitbucket.org/repo/Ko6rr5/images/4018744187-20130530_1600_heatmap_Gauss_Amp_final.jpeg)

![20130530_1600_heatmap_Gauss_Amp_final_dogbox.jpeg](https://bitbucket.org/repo/Ko6rr5/images/3147422613-20130530_1600_heatmap_Gauss_Amp_final_dogbox.jpeg)


***
***

## An Overview of how to generate heatmaps for a region. ##

1. Data is downloaded from the VSO for the desired region.
   - so far we have been working with either six or twelve-hour timespans, at 
     12 or 24-second cadence depending on what is available per particular wavelength.

2. The .fits files are first read into python.  The desired region is then specified and extracted from each file, put into a datacube, and the datacube is then de-rotated.  (The region's dimensions must take into account the trimming effect of the de-rotation.)

3. The image data is extracted from the cube and normalized by dividing through by the exposure time of each image.  The time-range of the dataset is also extracted, and both are saved in an HDF5 file.  

4. The intensity value for each pixel is extracted, and the timeseries is split into two-hour segments.  Each of these segments are processed through the Fast Fourier Transform, and then are averaged together to reduce the noise of the spectra.

5. To further reduce the noise and allow for a better fit, the region is then looped through by 3x3 pixel box.  The nine spectra in each are geometrically averaged and the result is assigned to the central pixel in the box.  

6. Each pixel's spectra is then fit to two models: the first made from a power-law-with-tail, and the second including an additional Gaussian component.

7. The six parameters from the combined model are extracted from the fits, as well as the chi-squared statistic.  In addition, the combined model fit and the averaged spectra are saved - to check in case of any errors.  

8. For each parameter, a heatmap is generated over the full region.


***
***

# A brief description on the organization of this repository + how the scripts should be used. #

### Main Folder: ###

- **download_fits.sh** : download .FITS files

- **derotate_slice_mpi2.py** : create datacube + derotate slices

- **merge_derotate_chunks.py** : merge the slices saved in previous file

- **heatmap_spectra_tool.py** : GUI for investigating individual pixel's spectra + fits

- **SolSpecShell.sh** : takes derotated cube + computes power spectra + memory maps result for fitting + fits spectra


### 'code_figures' Folder: ###

- scripts generating figures for the paper / presentation


### 'other_code' Folder: ###

- random other scripts, including older programs and ones I'm working on


### 'testing' Folder: ###

- scripts used to test various parts of the overall program


### 'validation' Folder: ###

- scripts generating spectra + fits to be cross-validated 


### 'visualizations' Folder: ###

- scripts generating some type of visualization used to analyze data