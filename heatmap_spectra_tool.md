*Here is the heatmap - spectra, side-by-side tool.*

*This image shows how clicking on one of the 'outlier' pixels shows that the cause of the low slope value 
was the fit not correctly fitting the low-frequency end of the spectra.*
![2016-12-30_11-56-01.png](https://bitbucket.org/repo/Ko6rr5/images/2943074095-2016-12-30_11-56-01.png)

*When clicking on a region of higher-slopes, the spectra illustrates the result.*
![2016-12-30_11-57-49.png](https://bitbucket.org/repo/Ko6rr5/images/2271288010-2016-12-30_11-57-49.png)

*A region that looks like it actually should have low slopes, the spectra confirms this.*
![2016-12-30_11-58-49.png](https://bitbucket.org/repo/Ko6rr5/images/1099387286-2016-12-30_11-58-49.png)

*The next two screenshots are of low and high power-law-tail values - again - clearly displayed on the spectra*
*(Obviously the colorbar scaling needs some work)*
![2016-12-30_12-02-11.png](https://bitbucket.org/repo/Ko6rr5/images/1166813315-2016-12-30_12-02-11.png)

![2016-12-30_12-03-01.png](https://bitbucket.org/repo/Ko6rr5/images/1739113659-2016-12-30_12-03-01.png)

*These next two just show how you can click back and forth between the different parameters and the visual image.*
![2016-12-30_12-04-05.png](https://bitbucket.org/repo/Ko6rr5/images/3256943167-2016-12-30_12-04-05.png)

![2016-12-30_12-05-02.png](https://bitbucket.org/repo/Ko6rr5/images/3128806046-2016-12-30_12-05-02.png)

*Finally, these two show how you can use the min/max slider to specify a custom range of values to display
on the heatmap.*
![2016-12-30_12-07-27.png](https://bitbucket.org/repo/Ko6rr5/images/1653627588-2016-12-30_12-07-27.png)

![2016-12-30_12-06-42.png](https://bitbucket.org/repo/Ko6rr5/images/3765034848-2016-12-30_12-06-42.png)
