# -*- coding: utf-8 -*-
"""
Created on Fri Sep 02 21:06:10 2016

@author: Brendan
"""

##########################################
### Check 180-second Sine Wave Results ###
##########################################

import scipy.signal
import numpy as np
import matplotlib.pyplot as plt


### Generate Sine Wave with 180 second Period
period = 180 # seconds
frequency = 1.0 / period # Hertz
omega = 2. * np.pi * frequency # radians per second
phi = 0.5 * np.pi # radians

N = 780 # number of samples we're dealing with
dt = 24 # 24 seconds each sample

timesteps = np.linspace(0.0, N*dt, N)
signal = np.sin(omega * timesteps + phi)

### Plot the Sine save
"""
# plot the signal
plt.figure(figsize=(14,4))
plt.plot(timesteps, signal)
plt.xlabel('Time (seconds)')
plt.ylabel('Signal Value')
"""


### Take a sampling of the points on the wave (dont need, already sampled)
"""
frac_points = 0.05 # fraction of points to select
r = np.random.rand(signal.shape[0])
timesteps_subset = timesteps[r >= (1.0 - frac_points)]
signal_subset = signal[r >= (1.0 - frac_points)]
"""

### Plot the 'moth-eaten sine wave'
"""
plt.figure(figsize=(14,4))
plt.plot(timesteps, signal, 'k', alpha=0.3, label="Original signal")
plt.plot(timesteps_subset, signal_subset, 'b+', label="Points kept in new signal")
plt.xlabel('Time (seconds)')
plt.ylabel('Signal Value')
"""


shffl_array = [0 for i in range(0,10)]
baseline = [0 for i in range(0,780)]

### shuffle the data and calculate the periodograms for each, take average and make baseline ###

# Choosing points in frequency space depends on what frequencies we think we'll detect.
# There's no point looking for 100 Hz signals when we're looking at T = 3 days periods.
for i in range(0,10):
    np.random.shuffle(signal)

    nout = 780 # number of frequency-space points at which to calculate the signal strength (output)
    periods = np.linspace(24, 780, nout)
    freqs = 1.0 / periods
    angular_freqs = 2 * np.pi * freqs

    pgram = scipy.signal.lombscargle(timesteps, signal, angular_freqs)

    plt.plot(periods, pgram)
    plt.xlabel('Period $T$ (seconds)')
    plt.ylabel('Power')
    
    plt.savefig('D:/SDO/Shuffled180Sine/shuffledfigure%i.jpeg' % (i))
    plt.close()
    
    with open('D:/SDO/Shuffled180Sine/shuffledsine180signaldata%i.txt' % (i), 'a') as f:
        np.savetxt(f,signal)
        f.closed
    shffl_array[i] = pgram
    
    with open('D:/SDO/Shuffled180Sine/shuffledsine180pgramdata%i.txt' % (i), 'a') as f:
        np.savetxt(f,pgram)
        f.closed


for j in range(0,780):
    for i in range(0,10):
        baseline[j] += shffl_array[i][j]

baseline = np.array(baseline)
baseline = baseline / 10

plt.plot(periods, baseline)
plt.xlabel('Period $T$ (seconds)')
plt.ylabel('Power')

plt.savefig('D:/SDO/Shuffled180Sine/baselineSine180.jpeg')
plt.close()

with open('D:/SDO/Shuffled180Sine/baselineSine180.txt', 'a') as f:
    np.savetxt(f,baseline)
    f.closed

### below is plotting original sine wave vs baseline

### Reset values to original and plot periodogram vs baseline
signal = np.sin(omega * timesteps + phi)

pgram = scipy.signal.lombscargle(timesteps, signal, angular_freqs)

plt.plot(periods, pgram)
plt.plot(periods, baseline)
plt.xlabel('Period $T$ (seconds)')
plt.ylabel('Power')

plt.savefig('D:/SDO/Shuffled180Sine/baselinevsOriginalSine180.jpeg')
plt.close()

### Shuffle data one time and plot vs baseline
np.random.shuffle(signal)

pgram = scipy.signal.lombscargle(timesteps, signal, angular_freqs)

plt.plot(periods, pgram)
plt.plot(periods, baseline)
plt.xlabel('Period $T$ (seconds)')
plt.ylabel('Power')

plt.savefig('D:/SDO/Shuffled180Sine/baselinevsShuffledSine180.jpeg')
plt.close()
