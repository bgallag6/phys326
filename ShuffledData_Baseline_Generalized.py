# -*- coding: utf-8 -*-
"""
Created on Fri Sep 02 21:06:10 2016

@author: Brendan
"""

##############################################################################
### I will add the values.txt and seconds.txt files to my repository.
### As you will see, there are a bunch of figures and files that are saved
### when this program is run - which must have valid paths specified for 
### them.  
##############################################################################




################################################
### Find Baseline of Shuffled Data + Compare ###
################################################

import scipy.signal
import numpy as np
import matplotlib.pyplot as plt


# read in data 
v = np.loadtxt('D:/SDO/values.txt', dtype='float')
t = np.loadtxt('D:/SDO/seconds.txt', dtype='float')

# create 5-step moving average
vmod = np.convolve(v, np.ones((5,))/5, mode='valid');

# find difference in lengths between moving average array and original 
l = (len(v)-len(vmod))

# delete the difference in lengths from the original (Prof. Weigel mentioned making the arrays
# the same length should be done differently - this was just for now)
v2 = np.delete(v,range(0,l))

# subtract moving average from original
diff = v2-vmod

# make time array same length as values 
t2 = np.delete(t,range(0,l))


### shuffle the data and calculate the periodograms for each

N = 1000

# initialize arrays for shuffled data and baseline
shffl_array = [0 for i in range(0,N)]
baseline_MAX = [0 for i in range(0,N)]
baseline_AVG = [0 for i in range(0,len(diff))]

# shuffle data N times
for i in range(0,N):
    np.random.shuffle(diff)
    
    # Choosing points in frequency space depends on what frequencies we think we'll detect.
    # There's no point looking for 100 Hz signals when we're looking at T = 3 days periods.
    nout = 776 # number of frequency-space points at which to calculate the signal strength (output)
    periods = np.linspace(24, 780, nout)
    freqs = 1.0 / periods
    angular_freqs = 2 * np.pi * freqs

    pgram = scipy.signal.lombscargle(t2, diff, angular_freqs)
    
    shffl_array[i] = pgram  # add shuffled data to array to use for finding AVG or MAX

#    plt.plot(periods, pgram)
#    plt.xlabel('Period $T$ (seconds)')
#    plt.ylabel('Power')
    
    # save periodogram to jpeg file
#    plt.savefig('D:/SDO/Shuffled180Sine/shuffledfigure%i.jpeg' % (i))
#    plt.close()
    
    # save shuffled data to text file
#    with open('D:/SDO/Shuffled180Sine/shuffledSunvaluesdata%i.txt' % (i), 'a') as f:
#        np.savetxt(f,diff)
#        f.closed
        
    # save shuffled periodogram power-values to text file
#    with open('D:/SDO/Shuffled180Sine/shuffledSunpgramdata%i.txt' % (i), 'a') as f:
#        np.savetxt(f,pgram)
#        f.closed


### Calculates the MAX baseline
        
for i in range(0,N):
    baseline_MAX[i] = np.amax(shffl_array[i])
baseline_MAX = np.array(baseline_MAX)
baseline_MAX = np.amax(baseline_MAX)
baseline_MAX = [baseline_MAX for i in range(0,len(diff))]


### Calculates the AVERAGED baseline

for j in range(0,len(diff)):
    for i in range(0,N):
        baseline_AVG[j] += shffl_array[i][j]
        
baseline_AVG = np.array(baseline_AVG)
baseline_AVG = baseline_AVG / N



### Plot the AVERAGED baseline and save to jpeg file as well as text file
"""
plt.plot(periods, baseline_AVG)
plt.xlabel('Period $T$ (seconds)')
plt.ylabel('Power')

plt.savefig('D:/SDO/Shuffled180Sine/baselineSun_AVG.jpeg')
plt.close()

with open('D:/SDO/Shuffled180Sine/baselineSun_AVG.txt', 'a') as f:
    np.savetxt(f,baseline_AVG)
    f.closed
"""

### Plot the MAX baseline and save to jpeg file as well as text file
"""
plt.plot(periods, baseline_MAX)
plt.xlabel('Period $T$ (seconds)')
plt.ylabel('Power')

plt.savefig('D:/SDO/Shuffled180Sine/baselineSun_MAX.jpeg')
plt.close()

with open('D:/SDO/Shuffled180Sine/baselineSun_MAX.txt', 'a') as f:
    np.savetxt(f,baseline_MAX)
    f.closed
"""


### Plot the baseline vs the original data periodogram, save as jpeg 

### Reset values to original
v = np.loadtxt('D:/SDO/values.txt', dtype='float')
vmod = np.convolve(v, np.ones((5,))/5, mode='valid');
l = (len(v)-len(vmod))
v2 = np.delete(v,range(0,l))
diff = v2-vmod

pgram = scipy.signal.lombscargle(t2, diff, angular_freqs)

# Plot original vs AVERAGED baseline
plt.plot(periods, pgram)
plt.plot(periods, baseline_AVG)
plt.xlabel('Period $T$ (seconds)')
plt.ylabel('Power')

plt.savefig('D:/SDO/Shuffled180Sine/baseline_AVGvsOriginalSun.jpeg')
plt.close()

# Plot original vs MAX baseline
plt.plot(periods, pgram)
plt.plot(periods, baseline_MAX)
plt.xlabel('Period $T$ (seconds)')
plt.ylabel('Power')

plt.savefig('D:/SDO/Shuffled180Sine/baseline_MAXvsOriginalSun.jpeg')
plt.close()

# Plot original vs AVERAGED and MAX baseline
plt.plot(periods, pgram)
plt.plot(periods, baseline_AVG)
plt.plot(periods, baseline_MAX)
plt.xlabel('Period $T$ (seconds)')
plt.ylabel('Power')

plt.savefig('D:/SDO/Shuffled180Sine/baseline_MAX_AVGvsOriginalSun.jpeg')
plt.close()



### Shuffle data one time and plot periodogram vs baseline, save as jpeg
np.random.shuffle(diff)

pgram = scipy.signal.lombscargle(t2, diff, angular_freqs)

# Plot shuffled vs AVERAGED baseline
plt.plot(periods, pgram)
plt.plot(periods, baseline_AVG)
plt.xlabel('Period $T$ (seconds)')
plt.ylabel('Power')

plt.savefig('D:/SDO/Shuffled180Sine/baseline_AVGvsShuffledSun.jpeg')
plt.close()

# Plot shuffled vs MAX baseline
plt.plot(periods, pgram)
plt.plot(periods, baseline_MAX)
plt.xlabel('Period $T$ (seconds)')
plt.ylabel('Power')

plt.savefig('D:/SDO/Shuffled180Sine/baseline_MAXvsShuffledSun.jpeg')
plt.close()

# Plot shuffled vs AVERAGED and MAX baseline
plt.plot(periods, pgram)
plt.plot(periods, baseline_AVG)
plt.plot(periods, baseline_MAX)
plt.xlabel('Period $T$ (seconds)')
plt.ylabel('Power')

plt.savefig('D:/SDO/Shuffled180Sine/baseline_MAX_AVGvsShuffledSun.jpeg')
plt.close()
