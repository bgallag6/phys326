# -*- coding: utf-8 -*-
"""
Created on Thu Sep 08 19:08:56 2016

@author: Brendan
"""
############################################################################
################################## Notes ###################################
###
###  This code attempts to address some of the major areas we were looking at.
###  It is just a test to see whether a broad-outline for looping over the full
###  sub-region will work, along with the comparison vs a calculated baseline and
###  recording the potential points of interest. 
###
###  So far, this code loops over overlapping 2x2 pixel boxes, which may be too much
###  work.  I think to change to adjacent boxes, you could change x1_box = i to 
###  x1_box = 2*i (as well as the other 3 pixel designators)		
###		
###  I did not address the normalizing of the images by its exposure time		
###
#############################################################################
##############################################################################

# AIA Analysis

# Import a bunch of stuff (some of these might not be needed...)
import scipy.signal
import matplotlib
#matplotlib.use('TkAgg') 	# NOTE: This is a MAC/OSX thing. Probably REMOVE for linux/Win
from matplotlib.widgets import  RectangleSelector
import matplotlib.pyplot as plt
from matplotlib.widgets import Cursor
from pylab import *
import glob
import sunpy
from sunpy.map import Map
from sunpy.image.coalignment import mapcube_coalign_by_match_template
from sunpy.physics.transforms.solar_rotation import mapcube_solar_derotate
from scipy.interpolate import interp1d
from scipy import signal
import numpy as np
import scipy.misc
import astropy.units as u
import h5py


with h5py.File("D:/SDO/SolarImageData_D.hdf5",'r') as f:  # Open HDF5 File
    
    DATA = f['Image Data']  # Retrieve Image Data from Dataset
    TIME = f['Time Data']  # Retrieve Time Data from Dataset
    
    
    pixmed=np.empty(DATA.shape[0])  # Initialize array to hold median pixel values
      
    found = np.array([[0,0,0,0,0]])  # Initialize array to hold found periodicities (x1,x2,y1,y2,period)
    #count = 0
    
    
    ##########################################
    ###   Loop over extracted sub-region   ###
    ##########################################
    
    for i in range(0,DATA.shape[2]-1):
       for j in range(0,DATA.shape[1]-1):
            
            #if (count == 100):
            #    break
            
            print i, j  # see where program is at
            
            ## 2501 / 2401 specified manually in this case - could be pulled from attribute? 
            i_cord = (2501 + i)  # determine (y) i-coordinate of overall 4096x4096 image
            j_cord = (2401 + j)  # determine (x) j-coordinate of overall 4096x4096 image
            
            ## pixel box to evaluate
            x1_box = i
            x2_box = i+2
            y1_box = j 
            y2_box = j+2
            
            for k in range(0,DATA.shape[0]):
                im=DATA[k]												# get image
                pixmed[k]=np.median(im[x1_box:x2_box,y1_box:y2_box])	# median
                
                
            # The derotation introduces some bad data towards the end of the sequence. This trims that off
            bad = np.argmax(pixmed <= 0.)		# Look for values <= zero
            last_good_pos = bad - 1				# retain only data before the <=zero
    
            # Get t and v data
            v=pixmed[0:last_good_pos]		
            t=TIME[0:last_good_pos]
                
            # create 5-step moving average
            vmod = np.convolve(v, np.ones((5,))/5, mode='valid');
            
            # find difference in lengths between moving average array and original 
            l = (len(v)-len(vmod))
            
            # delete the difference in lengths from the original (Prof. Weigel mentioned making the arrays
            # the same length should be done differently - this was just for now)
            v2 = np.delete(v,range(0,l))
            
            # subtract moving average from original
            diff = v2-vmod
            
            # dont know if needed - thought it solved a datatype error
            diff.astype(float)
            
            # make time array same length as values 
            t2 = np.delete(t,range(0,l))   
            
            # dont know if needed - thought it solved a datatype error
            t2 = t2.astype(float)
            
            
            ################################################################
            ### shuffle the data and calculate the periodograms for each ###
            ################################################################
           
            N = 1000
            
            # initialize arrays for shuffled data and baseline
            shffl_array = [0 for w in range(0,N)]
            baseline_MAX = [0 for w in range(0,N)]
            
            # shuffle data N times
            for n in range(0,N):
                
                np.random.shuffle(diff)
                
                # Determine the frequencies / periods to evaluate
                nout = len(t2) # number of frequency-space points at which to calculate the signal strength (output)
                periods = np.linspace(t[1], len(t), nout)
                freqs = 1.0 / periods
                angular_freqs = 2 * np.pi * freqs
            
                pgram = scipy.signal.lombscargle(t2, diff, angular_freqs)  # generate periodogram 
                
                shffl_array[n] = pgram  # add shuffled data to array to use for finding AVG or MAX
                        
            
            ### Calculates the baselines
                    
            for b in range(0,N):
                baseline_MAX[b] = np.amax(shffl_array[b])
            baseline_MAX = np.array(baseline_MAX)
            baseline_MAX_999 = np.percentile(baseline_MAX,99.9)  # 99.9% baseline
            baseline_MAX_99 = np.percentile(baseline_MAX,99)     # 99% baseline
            baseline_MAX_999 = [baseline_MAX_999 for r in range(0,len(diff))]
            baseline_MAX_99 = [baseline_MAX_99 for r in range(0,len(diff))]
                            
            
            diff = v2-vmod  # Reset pixel values to original 
            
            pgram = scipy.signal.lombscargle(t2, diff, angular_freqs)  # generate periodogram
            
            if (np.amax(pgram) > np.amax(baseline_MAX)):  # if max power exceeds baseline
                #count += 1
                
                # Plot the original data periodogram vs baselines, save as jpeg 
                              
                plt.figure(1)
                plt.suptitle('Pixel Location %ix, %iy' % (i_cord, j_cord), fontsize=14, fontweight='bold')
                
                ax1 = plt.subplot2grid((16,3), (0,0), rowspan=4, colspan=3) 
                plt.plot(t2, v2)
                ax1.set_xlim([0, t2[len(t2)-1]])
                plt.setp(ax1.get_xticklabels(), visible=False)
                plt.title('Raw (Above) / De-trended (Below) : Pixel Brightness vs. Time')
                plt.ylabel('Pixel Value')
                
                ax2 = plt.subplot2grid((16,3), (5,0), rowspan=4, colspan=3)
                plt.plot(t2, diff)
                ax2.set_xlim([0, t2[len(t2)-1]])
                plt.xlabel('Time (seconds)')
                plt.ylabel('Pixel Value')
                
                ax3 = plt.subplot2grid((16,3), (12,0), rowspan=4, colspan=3)
                plt.plot(periods, pgram)
                plt.plot(periods, baseline_MAX_999, label='1 in 1000')
                plt.plot(periods, baseline_MAX_99, label='1 in 100')
                plt.locator_params(axis='y',nbins=6)
                ax3.set_xlim([0, len(pgram)])
                plt.legend(loc='upper right', title='Likelihood of Occurrence', prop={'size':7})
                plt.title('Lomb-Scargle Periodogram')
                plt.xlabel('Period $T$ (seconds)')
                plt.ylabel('Power')
                
                plt.show()   
                #plt.savefig('F:/SDO/ScriptOutput/SignificantPeriodicity_Pixels%ix%iy.jpeg' % (i_cord,j_cord))
                plt.close()
                
                
                ## Store periods and pixel location to array                 
                """
                for f in range(0,len(t2)):
                    if pgram[f] > baseline_MAX[f]:
                        found1 = [[(x1_box),(x2_box),(y1_box),(y2_box),(periods[f])]]
                        found = np.append(found, found1, axis = 0)
                
                ## If periodicities are found, save array to text file
                if (found.shape[0] > 1):
                        with open('D:/SDO/SDO_data/results.txt', 'a') as f:
                            np.savetxt(f,found)
                """
                
f.close()  # close HDF5 file 
                  

            
            
           
