Add a file to your local repository and put it on Bitbucket
Go to your terminal window and navigate to the top level of your local repository.

[[$ cd ~/repos/bitbucketstationlocations/]]

1.	Get the status of your local repository. The git status command tells you about how your project is progressing 
	in comparison to your Bitbucket repository.  At this point, Git is aware that you created a new file, and you'll 
	see something like this:

[[$ git status 
On branch master
Initial commit
Untracked files:
  (use "git add <file>..." to include in what will be committed)
    locations.txt
nothing added to commit but untracked files present (use "git add" to track)]]

	The file is untracked, meaning that Git sees a file not part of a previous commit. The status output also shows you 
	the next step: adding the file.

2.	Tell Git to track your new locations.txt file using the git add command. Just like when you created a file, the git 
	add command doesn't return anything when you enter it correctly.

[[$ git add locations.txt]]

	The git add command moves changes from the working directory to the Git staging area. The staging area is where you 
	prepare a snapshot of a set of changes before committing them to the official history.

3.	Check the status of the file.

[[$ git status 
On branch master
Initial commit
Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
    new file: locations.txt]]

	Now you can see the new file has been added (staged) and you can commit it when you are ready. The git status command 
	displays the state of the working directory and the staged snapshot.

4.	Issue the git commit command with a commit message, as shown on the next line. The -m indicates that a commit message follows.

[[$ git commit -m 'Initial commit' 
[master (root-commit) fedc3d3] Initial commit
 1 file changed, 1 insertion(+)
 create mode 100644 locations.txt]]

	The git commit takes the staged snapshot and commits it to the project history. Combined with git add, this process defines 
	the basic workflow for all Git users.

	Up until this point, everything you have done is on your local system and invisible to your Bitbucket repository until you
	push those changes.
  
5.	Go back to your local terminal window and send your committed changes to Bitbucket using git push origin master. This command
	specifies that you are pushing to the master branch (the branch on Bitbucket) on origin (the Bitbucket server).
	You should see something similar to the following response:

[[$ git push origin master 
Counting objects: 3, done.
Writing objects: 100% (3/3), 253 bytes | 0 bytes/s, done.
Total 3 (delta 0), reused 0 (delta 0) Tohttps://emmap1@bitbucket.org/emmap1/bitbucketstationlocations.git
 * [new branch] master -> master
Branch master set up to track remote branch master from origin.]]

	Your commits are now on the remote repository (origin).


//////////////////////////////
//////////////////////////////
//////////////////////////////


Pull changes from your Git repository on Bitbucket Cloud

Step 2. Pull changes from a remote repository

	Now we need to get that new file into your local repository. The process is pretty straight forward, basically
	just the reverse of the push you used to get the  locations.txt  file into Bitbucket.  	To pull the file into 
	your local repository, do the following:

1.	Open your terminal window and navigate to the top level of your local repository.

[[$ cd ~/repos/bitbucketstationlocations/]]

2.	Enter the  git pull --all  command to pull all the changes from Bitbucket. (In more complex branching workflows, 
	pulling and merging all changes might not be appropriate .)  Enter your Bitbucket password when asked for it. 
	Your terminal should look similar to the following:

[[$ git pull --all
Fetching origin
remote: Counting objects: 3, done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
From https://bitbucket.org/emmap1/bitbucketstationlocations
   fe5a280..fcbeeb0 master -> origin/master
Updating fe5a280..fcbeeb0
Fast-forward
 stationlocations | 5 ++++++++++++++
 1 file changed, 5 insertions(+)
 create mode 100644 stationlocations]]

	The  git pull  command merges the file from your remote repository (Bitbucket) into your local repository with a single command. 

3.	Navigate to your repository folder on your local system and you'll see the file you just added.


//////////////////////////////
//////////////////////////////
//////////////////////////////


Use a Git branch to merge a file

Branches are most powerful when you're working on a team. You can work on your own part of a project from your own branch, 
pull updates from Bitbucket, and then merge all your work into the main branch when it's ready. Our documentation includes 
more explanation of why you would want to use branches.
A branch represents an independent line of development for your repository. Think of it as a brand-new working directory, 
staging area, and project history. Before you create any new branches, you automatically start out with the main branch (called  master ). 

Step 1. Create a branch and make a change

Create a branch where you can add future plans for the space station that you aren't ready to commit. When you are ready 
to make those plans known to all, you can merge the changes into your Bitbucket repository and then delete the no-longer-needed branch.
It's important to understand that branches are just  pointers  to commits. When you create a branch, all Git needs to do is 
create a new pointer�it doesn�t create a whole new set of files or folders.  

	To create a branch, do the following:

1.	Go to your terminal window and navigate to the top level of your local repository using the following command: 

[[$ cd ~/repos/bitbucketstationlocations/]]

2.	Create a branch from your terminal window.

[[$ git branch future-plans]]

	This command creates a branch but does not switch you to that branch.
 
	The repository history remains unchanged. All you get is a new pointer to the current branch. 
	To begin working on the new branch, you have to check out the branch you want to use.

3.	Checkout the new branch you just created to start using it.

[[$ git checkout future-plans
Switched to branch 'future-plans']]

	The git checkout command works hand-in-hand with git branch . Because you are creating a branch to work on
	something new, every time you create a new branch (with git branch), you want to make sure to check it out 
	(with git checkout ) if you're going to use it. 

4.	Search for the bitbucketstationlocations folder on your local system and open it. You will notice there are no 
	extra files or folders in the directory as a result of the new branch.

5.	Open the stationlocations file using a text editor.

6.	Make a change to the file by adding another station location:

7.	Save and close the file. 

8.	Enter git status in the terminal window. You will see something like this:  

[[$ git status 
On branch future-plans
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)
    modified: stationlocations
no changes added to commit (use "git add" and/or "git commit -a")]]

	Notice the  On branch future-plans  line? If you entered  git status  previously, the line was On branch master because 
	you only had the one master branch. Before you stage or commit a change, always check this line to make sure the branch 
	where you want to add the change is checked out.

9.	Stage your file.

[[$ git add stationlocations]]

10.	Enter the git commit  command in the terminal window, as shown with the following:

[[$ git commit stationlocations -m 'making a change in a branch' 
[future-plans e3b7732] making a change in a branch
 1 file changed, 4 insertions(+)]]
 
	Now it's time to merge the change that you just made back into the master branch.


Step 2. Merge your branch: fast-forward merging
Because you created only one branch and made one change, use the fast-forward branch method to merge.  You can do a fast-forward merge 
because you have a linear path from the current branch tip to the target branch. Instead of �actually� merging the branches, all Git 
has to do to integrate the histories is move (i.e., �fast-forward�) the current branch tip up to the target branch tip. This effectively
combines the histories, since all of the commits reachable from the target branch are now available through the current one.

This branch workflow is common for short-lived topic branches with smaller changes and are not as common for longer-running features.
To complete a fast-forward merge do the following:

1.	Go to your terminal window and navigate to the top level of your local repository.

[[$ cd ~/repos/bitbucketstationlocations/]]

2.	Enter the git status command to be sure you have all your changes committed and find out what branch you have checked out.

[[$ git status 
On branch future-plans
nothing to commit, working directory clean]]
 
3.	Switch to the master branch.

[[$ git checkout master 
Switched to branch 'master'
Your branch is up-to-date with 'origin/master'.]]

4.	Merge changes from the future-plans branch into the master branch.  It will look something like this:

[[$ git merge future-plans 
Updating fcbeeb0..e3b7732
Fast-forward
 stationlocations | 4 ++++
 1 file changed, 4 insertions(+)]]

	You've essentially moved the pointer for the master branch forward to the current head.

5.	Because you don't plan on using future-plans anymore, you can delete the branch.

[[$ git branch -d future-plans 
Deleted branch future-plans (was e3b7732).]]

	When you delete future-plans, you can still access the branch from master using a commit id. For example, if you want 
	to undo the changes added from future-plans, use the commit id you just received to go back to that branch.

6.	Enter git status to see the results of your merge, which show that your local repository is one ahead of your remote repository. 

[[$ git status 
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
nothing to commit, working directory clean]]

	Next, we need to push all this work back up to Bitbucket, your remote repository. 


Step 3. Push your change to Bitbucket
You want to make it possible for everyone else to see the location of the new space station. To do so, you can push the current 
state of your local repository to Bitbucket.

Here's how to push your change to the remote repository:

1.	From the repository directory in your terminal window,  enter git push origin master to push the changes. It will result 
	in something like this:

[[$ git push origin master 
Counting objects: 3, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 401 bytes | 0 bytes/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To https://emmap1@bitbucket.org/emmap1/bitbucketstationlocations.git
   fcbeeb0..e3b7732 master -> master]]

2.	Click Commits and you can see the commit you made on your local system. Notice that the change keeps the same commit id 
	as it had on your local system. 
  
	You can also see that the line to the left of the commits list has a straight-forward path and shows no branches. That�s 
	because the future-plans branch never interacted with the remote repository, only the change we created and committed.

3.	Click Branches and notice that the page has no record of the branch either.

4.	Click Source, and then click the stationlocations file.
	You can see the last change to the file has the commit id you just pushed.

