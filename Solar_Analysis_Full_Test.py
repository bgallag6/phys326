# -*- coding: utf-8 -*-
"""
Created on Mon Sep 05 13:43:10 2016

@author: Brendan
"""

### ***Try with small pixel selection first?***

###### Notes #######
#
#  This code attempts to address some of the major areas we were looking at.
#  It is just a test to see whether a broad-outline for looping over the full
#  sub-region will work, along with the comparison vs a calculated baseline and
#  recording the potential points of interest. 
#
#  So far, this code loops over overlapping 2x2 pixel boxes, which may be too much
#  work.  I think to change to adjacent boxes, you could change x1_box = i to 
#  x1_box = 2*i (as well as the other 3 pixel designators)		
#		
#  I did not address the normalizing of the images by its exposure time		
#
#  Additionally, I did not record the position of any of the found points of interest
#  relative to the original 4096x4096 resolution.  This could possibly using the locations
#  of the click points for the rectangle selector (since there will only be one selection now)  
#  and the x1_box,x2_box,y1_box,y2_box values from the loop.  
# 
############################

# AIA Analysis

# Import a bunch of stuff (some of these might not be needed...)
import scipy.signal
import matplotlib
matplotlib.use('TkAgg') 	# NOTE: This is a MAC/OSX thing. Probably REMOVE for linux/Win
from matplotlib.widgets import  RectangleSelector
import matplotlib.pyplot as plt
from matplotlib.widgets import Cursor
from pylab import *
import glob
import sunpy
from sunpy.map import Map
from sunpy.image.coalignment import mapcube_coalign_by_match_template
from sunpy.physics.transforms.solar_rotation import mapcube_solar_derotate
from scipy.interpolate import interp1d
from scipy import signal
import numpy as np
import scipy.misc
import astropy.units as u

# Function for selecting pixels on the images
def onselect(eclick, erelease):
  global startpos
  global endpos
  startpos=[0,0]
  endpos=[0,0]
  #print(' startpos : (%f, %f)' % (eclick.xdata, eclick.ydata))
  #print(' endpos   : (%f, %f)' % (erelease.xdata, erelease.ydata))
  #print(' used button   : ', eclick.button)
  startpos = [eclick.xdata, eclick.ydata]
  endpos   = [erelease.xdata, erelease.ydata]
  

# function definition for doing the button-clicks
def toggle_selector(event):
  print(' Key pressed.')
  if event.key in ['Q', 'q'] and toggle_selector.ES.active:
    print(' EllipseSelector deactivated.')
    toggle_selector.RS.set_active(False)
  if event.key in ['A', 'a'] and not toggle_selector.ES.active:
    print(' EllipseSelector activated.')
    toggle_selector.ES.set_active(True)


# create a list of all the files. This is USER-DEFINED
flist = glob.glob('AR1243/SDO_193/aia*.fits')
nf = len(flist)


# Select the image that is the "middle" of our selection.
# We do this because the solar derotation algorithm operates centered on the 
# "middle" image
mid_file = np.int(np.floor(nf / 2))
m1 = Map(flist[mid_file])

# extract image data from Map
d1 = m1.data

# Display the image in a window and select a rectangle
fig = plt.figure()
ax = fig.add_subplot(111, axisbg='#FFFFCC')
ax.set_xlim(1, 4096)
ax.set_ylim(1, 4096)
im=plt.imshow(d1,cmap=plt.get_cmap('jet'), vmin=0, vmax=8000)
toggle_selector.ES = RectangleSelector(ax, onselect, drawtype='box')
connect('key_press_event', toggle_selector)
plt.show()

### NOTE: you have to close the image window before the routine continues

# Get defined rectangle coords as integers
x1=np.int(startpos[1])
x2=np.int(endpos[1])
y1=np.int(startpos[0])
y2=np.int(endpos[0])

# This is just in case we defined the rectangle "backwards"
if (x1 > x2): x2, x1 = x1, x2
if (y1 > y2): y1, y2 = y2, y1

# Now we can read the entire dataset using this region only

# Create an empty list
mc_list = []

# Use coordinates we just defined, extract the submaps from each AIA image, and store
# then in the empty list. This takes many minutes to complete.
print " "
print "Reading files and extracting submaps. This takes a while..."
print " "
for filename in flist:
	mc_list.append(Map(filename).submap([y1,y2]*u.pixel, [x1,x2]*u.pixel))  # ** SEE NOTE BELOW ***
	
# NOTE: the u.pixel means we're selecting data based on pixel coordinates. Alternate coordinates
#		would be degrees of solar latitude/longitude positions
	
# Create a new Map Cube object to hold our de-rotated data
new_mapcube = Map(mc_list, cube=True)
print ""
print "Creating derotated cube..."
print "Please wait..."

# Perform the derotation of the submaps. This take a while too.
dr = mapcube_solar_derotate(new_mapcube)

mid_subarray = dr[mid_file].data		# extract data
subarr_xdim = mid_subarray.shape[1]		# get xdim
subarr_ydim = mid_subarray.shape[0]		# get ymin

pixmed=np.empty(nf)
pixmean=np.empty(nf)
seconds = np.empty(nf)
t = dr[0].date
base_time=(t.hour * 60.)+(t.minute * 60.)+t.second # Date/Time of image zero

found = np.array([[0,0,0,0,0]])  # Initialize array to hold found periodicities (x1,x2,y1,y2,period)

for i in range(0,mid_subarray.shape[1]-1):
   for j in range(0,mid_subarray.shape[0]-1):
        
        x1_box = i
        x2_box = i+2
        y1_box = j 
        y2_box = j+2
        
        for k in range(0,nf):
            im=dr[k].data												# get image
            t = dr[k].date											# time info
            pixmean[k]=np.mean(im[x1_box:x2_box,y1_box:y2_box])	# mean
            pixmed[k]=np.median(im[x1_box:x2_box,y1_box:y2_box])	# median
            curr_time=(t.hour * 3600.)+(t.minute * 60.)+t.second	# time
            seconds[k] = curr_time - base_time
            
        # The derotation introduces some bad data towards the end of the sequence. This trims that off
        bad = np.argmax(pixmed <= 0.)		# Look for values <= zero
        last_good_pos = bad - 1				# retain only data before the <=zero

        # Get t and v data
        v=pixmed[0:last_good_pos]		
        t=seconds[0:last_good_pos]
            
        # create 5-step moving average
        vmod = np.convolve(v, np.ones((5,))/5, mode='valid');
        
        # find difference in lengths between moving average array and original 
        l = (len(v)-len(vmod))
        
        # delete the difference in lengths from the original (Prof. Weigel mentioned making the arrays
        # the same length should be done differently - this was just for now)
        v2 = np.delete(v,range(0,l))
        
        # subtract moving average from original
        diff = v2-vmod
        
        # make time array same length as values 
        t2 = np.delete(t,range(0,l))   
        
        
        ################################################################
        ### shuffle the data and calculate the periodograms for each ###
        ################################################################
       
        N = 20
        
        # initialize arrays for shuffled data and baseline
        shffl_array = [0 for i in range(0,N)]
        baseline_MAX = [0 for i in range(0,N)]
        baseline_AVG = [0 for i in range(0,len(diff))]
        
        # shuffle data N times
        for i in range(0,N):
            np.random.shuffle(diff)
            
            # Choosing points in frequency space depends on what frequencies we think we'll detect.
            # There's no point looking for 100 Hz signals when we're looking at T = 3 days periods.
            nout = len(t2) # number of frequency-space points at which to calculate the signal strength (output)
            periods = np.linspace(t[1], len(t), nout)
            freqs = 1.0 / periods
            angular_freqs = 2 * np.pi * freqs
        
            pgram = scipy.signal.lombscargle(t2, diff, angular_freqs)
            
            shffl_array[i] = pgram  # add shuffled data to array to use for finding AVG or MAX
        
        
        ### Calculates the MAX baseline
                
        for i in range(0,N):
            baseline_MAX[i] = np.amax(shffl_array[i])
        baseline_MAX = np.array(baseline_MAX)
        baseline_MAX = np.amax(baseline_MAX)
        baseline_MAX = [baseline_MAX for i in range(0,len(diff))]
        
        
        ### Calculates the AVERAGED baseline
        
        for j in range(0,len(diff)):
            for i in range(0,N):
                baseline_AVG[j] += shffl_array[i][j]
                
        baseline_AVG = np.array(baseline_AVG)
        baseline_AVG = baseline_AVG / N
        
                        
        ### Reset values to original
        diff = v2-vmod
        
        pgram = scipy.signal.lombscargle(t2, diff, angular_freqs)
        
        if (np.amax(pgram) > np.amax(baseline_MAX)):
            for i in range(0,len(t2)):
                if pgram[i] > baseline_MAX[i]:
                    found1 = [[(x1_box),(x2_box),(y1_box),(y2_box),(periods[i])]]
                    found = np.append(found, found1, axis = 0)
                    
#####################################################################################                    
### The commands below will create way too many files when used with the full     ###
### program.  I'm including them just in case we wanted to verify a small section ###
### of the image once/if something is found.                                         ###
#####################################################################################
                    
        # Plot original vs AVERAGED and MAX baseline + save as jpeg
        """ 
        ### PROBABLY TOO MANY FILES, MAYBE USE TO REVIEW SMALL SECTION OF IMAGE
        plt.plot(periods, pgram)
        plt.plot(periods, baseline_AVG)
        plt.plot(periods, baseline_MAX)
        plt.xlabel('Period $T$ (seconds)')
        plt.ylabel('Power')
        
        plt.savefig('___user_defined_path___')
        plt.close()
        """
              
        ### Shuffle data one time and plot periodogram vs baseline, save as jpeg
        """ DONT NECESSARILY NEED - USED TO SHOW SIGNIFICANCE OF FOUND PERIODICITY VS RANDOM
        np.random.shuffle(diff)
        
        pgram = scipy.signal.lombscargle(t2, diff, angular_freqs)
        """
                
        # Plot shuffled vs AVERAGED and MAX baseline + save as jpeg
        """ 
        ### PROBABLY TOO MANY FILES, MAYBE USE TO REVIEW SMALL SECTION OF IMAGE
        plt.plot(periods, pgram)
        plt.plot(periods, baseline_AVG)
        plt.plot(periods, baseline_MAX)
        plt.xlabel('Period $T$ (seconds)')
        plt.ylabel('Power')
        
        plt.savefig('___user_defined_path___')
        plt.close()
        """

### If significant periodicities are found, save to text file
if (found.shape[0] > 1):
        
        with open('___user_defined_path___', 'a') as f:
            np.savetxt(f,found)
            f.closed
            

                    
        